import java.util.Scanner;

public class LuasSegitiga {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        double a, t, l;
        System.out.println("Program Luas Segitiga (branch coba-1)");
        System.out.print("Alas : ");
        a = keyboard.nextDouble();
        System.out.print("Tinggi : ");
        t = keyboard.nextDouble();

        l = hitungLuasSegitiga(a, t);

        System.out.println("Luas alas adalah  : " + l);

    }

    public static double hitungLuasSegitiga(double a, double t) {
        double l;
        l = 1 / 2 * a * t;
        return l;
    }
}